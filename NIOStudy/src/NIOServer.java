import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.nio.ByteBuffer;
import java.nio.channels.*;
import java.security.Key;
import java.util.Iterator;
import java.util.Set;

public class NIOServer {


    public static void main(String[] args) {
        try {
            ServerSocketChannel channel = ServerSocketChannel.open();
            channel.configureBlocking(false);
            InetSocketAddress socketAddress = new InetSocketAddress( 8010);
            ServerSocket serverSocket = channel.socket();
            serverSocket.bind(socketAddress);
            Selector selector = Selector.open();
            channel.register(selector, SelectionKey.OP_ACCEPT);



            while (true){
                try {
                    selector.select();
                    Set<SelectionKey> selectionKeys = selector.selectedKeys();
                    Iterator<SelectionKey> iterator = selectionKeys.iterator();
                    while (iterator.hasNext()){
                        SelectionKey next = iterator.next();
                        iterator.remove();
                        if (next.isAcceptable()){
                            ServerSocketChannel server = (ServerSocketChannel) next.channel();
                            SocketChannel client = server.accept();
                            client.configureBlocking(true);
                            SelectionKey clientKey = client.register(selector, SelectionKey.OP_READ | SelectionKey.OP_WRITE);
                            ByteBuffer byteBuffer = ByteBuffer.allocate(100);
                            clientKey.attach(byteBuffer);
                        }
                        if (next.isReadable()){
                          SocketChannel client = (SocketChannel) next.channel();
                          ByteBuffer output = (ByteBuffer) next.attachment();
                          client.read(output);
                        }
                        if (next.isWritable()){
                            SocketChannel client = (SocketChannel) next.channel();
                            ByteBuffer output = (ByteBuffer) next.attachment();
                            output.flip();
                            client.write(output);
                            output.compact();
                        }

                    }

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }


        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
