import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.MulticastSocket;
import java.net.SocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.DatagramChannel;

public class UDPTest {


    public static void main(String[] args) throws IOException {
        DatagramChannel channel = DatagramChannel.open();
        InetAddress inetAddress = InetAddress.getByName("localhost");
        InetSocketAddress socketAddress = new InetSocketAddress(inetAddress, 9001);
        channel.bind(socketAddress);

        ByteBuffer byteBuffer = ByteBuffer.allocate(128);
        SocketAddress client = channel.receive(byteBuffer);

        byteBuffer.flip();

        channel.send(byteBuffer, client);



        MulticastSocket multicastSocket = new MulticastSocket();
        // 加入
        multicastSocket.joinGroup(inetAddress);
        // 离开
        multicastSocket.leaveGroup(inetAddress);
    }

}
