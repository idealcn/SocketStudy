import multicast.MulticastReadTask;
import multicast.MulticastWriteTask;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

public class MultiCastTest2 {

    public static void main(String[] args) throws IOException {
        MulticastSocket multicastSocket = new MulticastSocket(8991);
        InetAddress inetAddress = InetAddress.getByName("239.255.2.2");
        multicastSocket.joinGroup(inetAddress);


        ScheduledExecutorService threadPool = Executors.newScheduledThreadPool(5);
        threadPool.submit(new MulticastReadTask(multicastSocket));
        threadPool.submit(new MulticastWriteTask(multicastSocket));





        byte[] buffer = new byte[128];
        DatagramPacket send = new DatagramPacket(buffer, buffer.length);


        DatagramPacket receive = new DatagramPacket(buffer, buffer.length);

        boolean hasRead = false;

        while (!hasRead){
            multicastSocket.send(send);


        }

    }
}
