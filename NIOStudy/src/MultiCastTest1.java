import multicast.MulticastReadTask;
import multicast.MulticastWriteTask;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

public class MultiCastTest1 {

    public static void main(String[] args)  {
        try {
            MulticastSocket multicastSocket = new MulticastSocket(49999);
            InetAddress inetAddress = InetAddress.getByName("239.0.0.0");
            multicastSocket.joinGroup(inetAddress);


            ScheduledExecutorService threadPool = Executors.newScheduledThreadPool(5);
            threadPool.submit(new MulticastReadTask(multicastSocket));
            threadPool.submit(new MulticastWriteTask(multicastSocket));
        } catch (IOException e) {
            e.printStackTrace();
        }


    }
}
