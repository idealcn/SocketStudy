import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.nio.channels.SocketChannel;
import java.nio.channels.WritableByteChannel;

public class NIOClient {


    public static void main(String[] args) {


        try {
            InetSocketAddress socketAddress = new InetSocketAddress("localhost", 8010);
            SocketChannel client = SocketChannel.open(socketAddress);


            if (client.isConnected() && client.isOpen()){
                WritableByteChannel channel = Channels.newChannel(System.out);
                ByteBuffer byteBuffer = ByteBuffer.allocate(16);
                int read = client.read(byteBuffer);
                while (read!=-1){
                    byteBuffer.flip();
                    channel.write(byteBuffer);
                    byteBuffer.clear();
                    read = client.read(byteBuffer);
                }

            }


        } catch (IOException e) {
            e.printStackTrace();
        }


    }


}
