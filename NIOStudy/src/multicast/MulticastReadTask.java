package multicast;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.MulticastSocket;

public class MulticastReadTask implements Runnable{


    private MulticastSocket reader;
    public MulticastReadTask(MulticastSocket multicastSocket) {
        this.reader = multicastSocket;
    }

    @Override
    public void run() {
        boolean done = false;
        byte[] bytes = new byte[128];
        DatagramPacket receive = new DatagramPacket(bytes, bytes.length);
        while (!done){
            try {
                //阻塞方法
                reader.receive(receive);
                byte[] receiveData = receive.getData();
                String message = new String(receiveData, 0, receiveData.length);
                System.out.println("收到来自："+reader.getInetAddress().getHostAddress()+":"+reader.getPort()+",消息："+message);

            } catch (IOException e) {
                e.printStackTrace();
                done = true;
            }
        }

        reader.close();
    }
}
