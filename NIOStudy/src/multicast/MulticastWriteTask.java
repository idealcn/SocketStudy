package multicast;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.DatagramPacket;
import java.net.MulticastSocket;

public class MulticastWriteTask implements Runnable{


    private MulticastSocket writer;
    public MulticastWriteTask(MulticastSocket multicastSocket) {

        this.writer = multicastSocket;
        try {
            writer.setTimeToLive(1);
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    @Override
    public void run() {

        boolean done = false;
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        while (!done) {

            String line = null;
                try {
                    if ((line = reader.readLine())!=null){
                        byte[] bytes = line.getBytes();
                        DatagramPacket packet = new DatagramPacket(bytes, bytes.length);
                        packet.setAddress(writer.getInetAddress());
                        packet.setPort(writer.getPort());
                        writer.send(packet);
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                    done = true;
                }


        }

        try {
            writer.close();
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
