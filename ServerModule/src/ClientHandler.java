import java.io.*;
import java.net.Socket;

public class ClientHandler implements Runnable {

    private Socket client;
    public ClientHandler(Socket client) {
        this.client = client;
    }

    @Override
    public void run() {
        BufferedReader reader = null;
        boolean exit = false;
        InputStream inputStream = null;
        PrintStream printStream = null;
        while (!exit){
            try {
                inputStream = client.getInputStream();
                printStream = new PrintStream(client.getOutputStream());
                reader = new BufferedReader(new InputStreamReader(inputStream));
                String line = reader.readLine();
                System.out.println("接收到新消息： "+line);
                if ("bye".equals(line)){
                    exit = true;
                    printStream.write("bye".getBytes());
                }
            } catch (IOException e) {
                e.printStackTrace();
            }finally {
//                ServerIOCloseUtil.close(reader);
//                ServerIOCloseUtil.close(inputStream);
//                ServerIOCloseUtil.close(printStream);
            }
        }
    }
}
