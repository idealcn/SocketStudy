import java.io.Closeable;
import java.io.IOException;

public class IOCloseUtil {

    public static void close(Closeable io){
        if (null!=io){
            try {
                io.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

}
