import java.io.*;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {

    public static void main(String[] args) {
        try {
            ThreadTool threadTool = ThreadTool.getInstance();
            ServerSocket serverSocket = new ServerSocket();
            serverSocket.bind(new InetSocketAddress(9100));
            while (true) {
                Socket socket = serverSocket.accept();
                if (socket.isConnected() && !socket.isClosed()) {
                    threadTool.submit(new ReaderHandler(socket));
                    threadTool.submit(new WriterHandler(socket));
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
