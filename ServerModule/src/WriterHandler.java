import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.Socket;

public class WriterHandler implements Runnable{

    private Socket client;

    public WriterHandler(Socket client) {
        this.client = client;
    }

    @Override
    public void run() {
        PrintStream printStream = null;
        BufferedReader reader = null;
        try {
            printStream = new PrintStream(client.getOutputStream());
            reader = new BufferedReader(new InputStreamReader(System.in));
            String line;
            while ((line=reader.readLine())!=null){
                printStream.println(line);
                printStream.flush();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            IOCloseUtil.close(printStream);
            IOCloseUtil.close(reader);
            try {
                client.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
