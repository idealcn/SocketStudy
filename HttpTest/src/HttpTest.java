
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.*;
import java.security.Permission;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class HttpTest {

    public static void main(String[] args) {
        try {
            InetAddress[] address = InetAddress.getAllByName("www.baidu.com");
            for (InetAddress inetAddress : address) {
                System.out.println("host => "+inetAddress.getHostName()+", hostAddress =>  "+inetAddress.getHostAddress());
                boolean reachable = inetAddress.isReachable(1000);
                System.out.println("reach ==> "+reachable);
            }
            InetAddress localHost = InetAddress.getLocalHost();
            System.out.println(" localHost => "+localHost);
            System.out.println("-------------------------");
            NetworkInterface networkInterface = NetworkInterface.getByName("www.baidu.com");
            if (null!=networkInterface){
                String displayName = networkInterface.getDisplayName();
                String hardwareAddress = new String(networkInterface.getHardwareAddress());
                System.out.println("displayName => "+displayName+", hardwareAddress => "+hardwareAddress);

                List<InterfaceAddress> interfaceAddressList = networkInterface.getInterfaceAddresses();
                for (InterfaceAddress interfaceAddress : interfaceAddressList) {
                    InetAddress inetAddress = interfaceAddress.getAddress();
                    InetAddress broadcast = interfaceAddress.getBroadcast();
                    System.out.println("address => "+inetAddress.getHostAddress()+",name => "+inetAddress.getHostName());
                    System.out.println("broadcast: name => "+broadcast.getHostName()+", address => "+broadcast.getHostAddress());
                    System.out.println("-------------        -----------");
                }

                SocketAddress socketAddress = new InetSocketAddress("www.baidu.com",80);
                Proxy proxy = new Proxy(Proxy.Type.DIRECT,socketAddress);
                ProxySelector proxySelector = ProxySelector.getDefault();

                Authenticator authenticator = new Authenticator() {
                    @Override
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return super.getPasswordAuthentication();
                    }

                    @Override
                    protected URL getRequestingURL() {
                        return super.getRequestingURL();
                    }

                    @Override
                    protected RequestorType getRequestorType() {
                        return super.getRequestorType();
                    }
                };
                Authenticator.setDefault(authenticator);

                PasswordAuthentication passwordAuthentication = new PasswordAuthentication("",new char[]{});
                String userName = passwordAuthentication.getUserName();

                int httpBadGateway = HttpURLConnection.HTTP_BAD_GATEWAY;


                CookieManager cookieManager  = new CookieManager();
                CookieStore cookieStore = cookieManager.getCookieStore();
                List<HttpCookie> cookieList = cookieStore.getCookies();
                CookieHandler.setDefault(cookieManager);

               HttpURLConnection connection = (HttpURLConnection) new URL("").openConnection();
               //查询某个首部字段
                String value = connection.getHeaderField("key");

                connection.setDoInput(true);
                connection.setDoOutput(true);
                connection.setRequestMethod("POST");
                connection.setRequestProperty("Connection","keep-alive");
                connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");

                //getOutputStream()会检测是否连接并进行connect()
                OutputStream outputStream = connection.getOutputStream();
                // CacheRequest cacheRequest = new ca
                String message = "name=http&pwd=123";
                //写入数据
                outputStream.write(URLEncoder.encode(message,"utf-8").getBytes());

                Permission permission = connection.getPermission();

                InputStream errorStream = connection.getErrorStream();


            }

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}
