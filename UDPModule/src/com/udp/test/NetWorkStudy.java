package com.udp.test;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Enumeration;

public class NetWorkStudy {


    public static void main(String[] args) {
        try {
            NetworkInterface networkInterface = NetworkInterface.getByName("127.0.0.1");
            if (null == networkInterface ) {
                networkInterface = NetworkInterface.getByInetAddress(InetAddress.getLocalHost());
            }
            if (null == networkInterface){
                return;
            }
            String displayName = networkInterface.getDisplayName();
            byte[] hardwareAddress = networkInterface.getHardwareAddress();
            String address = new String(hardwareAddress);
            int mtu = networkInterface.getMTU();
            String name = networkInterface.getName();
            Enumeration<InetAddress> enumeration = networkInterface.getInetAddresses();
            while (enumeration.hasMoreElements()){
                InetAddress inetAddress = enumeration.nextElement();
                String hostAddress = inetAddress.getHostAddress();
                String hostName = inetAddress.getHostName();
                boolean multicastAddress = inetAddress.isMulticastAddress();

            }
        } catch (SocketException | UnknownHostException e) {
            e.printStackTrace();
        }
    }
}
