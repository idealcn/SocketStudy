package com.udp.test;

import java.net.DatagramPacket;

public interface OnMessageCallback {

    void onMessageReceived(DatagramPacket receive);

}
