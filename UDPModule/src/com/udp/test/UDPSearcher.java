package com.udp.test;

import com.udp.test.client.ClientReadHandler;
import com.udp.test.client.ClientWriteHandler;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class UDPSearcher {



    public static void main(String[] args) throws IOException {

        DatagramSocket searcher = new DatagramSocket();
//        byte[] buff = "hello com.udp.test.server".getBytes();
//        DatagramPacket packet = new DatagramPacket(buff,buff.length);
//        packet.setAddress(InetAddress.getByName("255.255.255.255"));//255.255.255.255
//        packet.setPort(9200);
//       // searcher.setBroadcast(true);
//        searcher.send(packet);
//
//        byte[] receiveData = new byte[64];
//        DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);
//        searcher.receive(receivePacket);
//        System.out.println("接收到："+new String(receiveData));

        ExecutorService mThreadPoolExecutor = Executors.newScheduledThreadPool(Runtime.getRuntime().availableProcessors());
        ClientReadHandler clientReadHandler = new ClientReadHandler(searcher,mThreadPoolExecutor);
        clientReadHandler.start();


        ClientWriteHandler clientWriteHandler = new ClientWriteHandler(searcher,mThreadPoolExecutor);
        clientWriteHandler.start();



//        int port = 9200;
//        InetAddress inetAddress = InetAddress.getByName("255.255.255.255");
//
//        boolean done = false;
//        BufferedReader reader = null;
//        DatagramPacket dataPacket;
//        while (!done){
//
//            reader = new BufferedReader(new InputStreamReader(System.in));
//            String line = reader.readLine();
//            if (line!=null){
//                byte[] buffer = line.getBytes();
//                dataPacket = new DatagramPacket(buffer,0,buffer.length);
//                dataPacket.setAddress(inetAddress);
//                dataPacket.setPort(port);
//                searcher.send(dataPacket);
//            }
//
//
//
//
//        }


    }

}
