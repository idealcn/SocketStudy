package com.udp.test.client;

import com.udp.test.UDPConstant;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.concurrent.ExecutorService;

public class ClientReadHandler {


    private DatagramSocket client;
    private ExecutorService mThreadPoolExecutor;

    public ClientReadHandler(DatagramSocket client, ExecutorService mThreadPoolExecutor) {
        this.client = client;
        this.mThreadPoolExecutor = mThreadPoolExecutor;
    }


    public void send(String message){

    }

    public void close() {
        client.close();
        mThreadPoolExecutor.shutdown();
    }

    public void response(String response) throws IOException {

        final byte[] buffer = response.getBytes();
        DatagramPacket packet = new DatagramPacket(buffer,buffer.length);
        packet.setPort(UDPConstant.PORT);
        packet.setAddress(InetAddress.getByName(UDPConstant.ADDRESS));
        client.send(packet);

    }

    public void start() {
        mThreadPoolExecutor.execute(new ClientReadTask(client, this));
    }
}
