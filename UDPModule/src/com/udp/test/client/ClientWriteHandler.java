package com.udp.test.client;

import java.net.DatagramSocket;
import java.util.concurrent.ExecutorService;

public class ClientWriteHandler {


    private DatagramSocket writer;
    private ExecutorService mThreadPoolExecutor;

    public ClientWriteHandler(DatagramSocket writer,ExecutorService mThreadPoolExecutor) {
        this.writer = writer;
        this.mThreadPoolExecutor = mThreadPoolExecutor;
    }




    public void start() {
        ClientWriteTask task = new ClientWriteTask(writer,this);
        //task.write("");
        mThreadPoolExecutor.execute(task);
    }


    public void close() {
        writer.close();
        mThreadPoolExecutor.shutdown();
    }
}
