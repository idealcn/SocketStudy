package com.udp.test.client;

import com.udp.test.UDPConstant;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

public class ClientReadTask implements Runnable {



    private DatagramSocket client;
    private ClientReadHandler handler;
    public ClientReadTask(DatagramSocket client,ClientReadHandler handler) {
        this.client = client;
        this.handler = handler;
    }

    @Override
    public void run() {

        boolean done = false;
        byte[] buffer = new byte[128];
        DatagramPacket packet = new DatagramPacket(buffer,buffer.length);


        while (!done){
            try {
                client.receive(packet);
                InetAddress inetAddress = packet.getAddress();
                String hostAddress = inetAddress.getHostAddress();
                int port = packet.getPort();
                byte[] bytes = packet.getData();
                String message = new String(bytes,0,bytes.length);
                System.out.println("receive from address: "+hostAddress+",port: "+port+", message: "+message);
                if (message.equalsIgnoreCase("bye")){
                    done = true;
                    handler.close();
                }else  {
                   // handler.response("i got your message");
                }
            } catch (IOException e) {
                e.printStackTrace();
                done = true;
                handler.close();
            }
        }


    }
}
