package com.udp.test.client;

import com.udp.test.UDPConstant;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.UnknownHostException;

public class ClientWriteTask implements Runnable {

    private DatagramSocket writer;
    private ClientWriteHandler handler;
    public ClientWriteTask(DatagramSocket writer,ClientWriteHandler handler) {
       this.writer = writer;
       this.handler = handler;
    }

    @Override
    public void run() {

        boolean done = false;
        BufferedReader reader = null;
        DatagramPacket dataPacket;
        InetAddress inetAddress = null;
        try {
            inetAddress = InetAddress.getByName(UDPConstant.ADDRESS);
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }

        while (!done){
            try {
                reader = new BufferedReader(new InputStreamReader(System.in));
                String line = reader.readLine();
                if (line!=null){
                    byte[] buffer = line.getBytes();
                    dataPacket = new DatagramPacket(buffer,0,buffer.length);
                    dataPacket.setAddress(inetAddress);
                    dataPacket.setPort(UDPConstant.PORT);
                    writer.send(dataPacket);
                    if (line.equalsIgnoreCase("bye")){
                        done = true;
                        handler.close();
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
                done = true;
                handler.close();
            }
        }
    }

}
