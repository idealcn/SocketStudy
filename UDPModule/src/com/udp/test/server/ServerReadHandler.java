package com.udp.test.server;

import com.udp.test.OnMessageCallback;

import java.net.DatagramSocket;
import java.util.concurrent.ExecutorService;

public class ServerReadHandler {


    private DatagramSocket server;
    private ExecutorService mThreadPoolExecutor;
    private OnMessageCallback onMessageCallback;

    public ServerReadHandler(DatagramSocket server, ExecutorService mThreadPoolExecutor, OnMessageCallback onMessageCallback) {
        this.server = server;
        this.mThreadPoolExecutor = mThreadPoolExecutor;
        this.onMessageCallback = onMessageCallback;
    }

    public void start() {
        mThreadPoolExecutor.submit(new ServerReadTask(server,this,onMessageCallback));
    }

    public void close() {
        server.close();
        mThreadPoolExecutor.shutdown();
    }
}
