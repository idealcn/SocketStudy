package com.udp.test.server;

import com.udp.test.OnMessageCallback;
import com.udp.test.UDPConstant;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

public class ServerReadTask implements Runnable {

    private DatagramSocket server;
    private ServerReadHandler handler;
    private OnMessageCallback onMessageCallback;


    public ServerReadTask(DatagramSocket server, ServerReadHandler serverReadHandler, OnMessageCallback onMessageCallback) {
        this.server = server;
        this.handler = serverReadHandler;
        this.onMessageCallback = onMessageCallback;
    }

    @Override
    public void run() {
        boolean done  = false;
        while (!done){
            try {
                System.out.println("服务端开始接收数据...");
                byte[] buffer = new byte[128];
                DatagramPacket packet = new DatagramPacket(buffer,buffer.length);
                server.receive(packet);

                System.out.println("服务端开始解析数据...");

                InetAddress inetAddress = packet.getAddress();
                int port = packet.getPort();
                byte[] bytes = packet.getData();
                String message = new String(bytes,0,bytes.length);
                System.out.println("来自："+inetAddress.getHostAddress()+":"+port);
                System.out.println("消息："+message);
                onMessageCallback.onMessageReceived(packet);

            } catch (IOException e) {
                e.printStackTrace();
                done = true;
                handler.close();
            }
        }
    }
}
