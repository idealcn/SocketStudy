package com.udp.test.server;

import com.udp.test.OnMessageCallback;

import java.io.IOException;
import java.net.*;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class UDPProvider {


    public static void main(String[] args) {


        try {
            DatagramSocket server = new DatagramSocket(9200);


            final ExecutorService mThreadPoolExecutor = Executors.newScheduledThreadPool(5);

            final ServerWriterHandler writerHandler = new ServerWriterHandler(server,mThreadPoolExecutor);
//            writerHandler.start();


            final Map<String,ServerWriterHandler> writerHandlerMap = new HashMap<>();

            //读消息
            ServerReadHandler readHandler = new ServerReadHandler(server,mThreadPoolExecutor,new OnMessageCallback(){

                @Override
                public void onMessageReceived(DatagramPacket receive) {
                    //收到消息后开始后回应。
//                    writerHandler.response(receive);
                    ServerWriterHandler handler = writerHandlerMap.get(receive.getAddress().getHostAddress());
                    if (handler == null){
                        handler = new ServerWriterHandler(server, mThreadPoolExecutor);
                        writerHandlerMap.put(receive.getAddress().getHostAddress(), handler);
                    }
                    handler.response(receive);
                }
            });
            readHandler.start();



//

            
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
