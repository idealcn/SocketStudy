package com.udp.test.server;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.util.concurrent.ExecutorService;

public class ServerWriterHandler {

    private DatagramSocket server;
    private ExecutorService mThreadPoolExecutor;

    public ServerWriterHandler(DatagramSocket server, ExecutorService mThreadPoolExecutor) {
        this.server = server;
        this.mThreadPoolExecutor = mThreadPoolExecutor;
    }

    public void start() {
      //  mThreadPoolExecutor.execute(new ServerWriterTask(server,this, receive));
    }


    public void response(DatagramPacket receive) {
        mThreadPoolExecutor.execute(new ServerWriterTask(server,this,receive));
    }

    public void close() {
        mThreadPoolExecutor.shutdown();
        server.close();
    }
}
