package com.udp.test.server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.DatagramPacket;
import java.net.DatagramSocket;

public class ServerWriterTask implements Runnable {


    private DatagramSocket server;
    private ServerWriterHandler handler;
    private DatagramPacket receive;
    public ServerWriterTask(DatagramSocket server, ServerWriterHandler serverWriterHandler, DatagramPacket receive) {
       this.server = server;
       this.handler = serverWriterHandler;
       this.receive  = receive;
    }

    @Override
    public void run() {
       boolean done= false;
       while (!done) {
           try {
               BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
               String line = reader.readLine();
               if (line!=null){
                   byte[] bytes = line.getBytes();

                   DatagramPacket packet = new DatagramPacket(bytes,bytes.length);
                   packet.setPort(receive.getPort());
                   packet.setAddress(receive.getAddress());
                   server.send(packet);
               }
               done = true;
           } catch (IOException e) {
               e.printStackTrace();
               done = true;
               handler.close();
           }
       }
    }
}
