import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;

public class ReaderHandler implements Runnable {
    private Socket client;
    public ReaderHandler(Socket client){
        this.client = client;
    }

    @Override
    public void run() {
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new InputStreamReader(client.getInputStream()));
            String line = null;
            while ((line=reader.readLine())!=null){
                System.out.println("接收到消息： "+line);
                if (line.equals("bye")){
                    client.getOutputStream().write("bye".getBytes());
                    client.close();
                    System.out.println("com.udp.test.client close");
                    break;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            IOCloseUtil.close(reader);
            try {
                client.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
