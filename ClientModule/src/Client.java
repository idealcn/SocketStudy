import java.io.*;
import java.net.*;
import java.util.Scanner;

public class Client {

    public static void main(String[] args) {
        try {
            //构造一个socket，但是不连接
            Socket socket = new Socket();
            //保持长连接，每隔一段时间发送一条消息，确保连接不会中断。
            socket.setKeepAlive(true);
            //设置超时时间
            //socket.setSoTimeout(10000);
            //立即发送数据，不必等到数据组包后再发送。
            socket.setTcpNoDelay(true);
            InetSocketAddress socketAddress = new InetSocketAddress( Inet4Address.getLocalHost(),9100);
            //连接
            socket.connect(socketAddress);
            //远程地址
            SocketAddress remoteSocketAddress = socket.getRemoteSocketAddress();
            if (remoteSocketAddress instanceof InetSocketAddress) {
                InetSocketAddress remote  = (InetSocketAddress) remoteSocketAddress;
                System.out.println("remote:  address => "+remote.getAddress()+",name => "+remote.getHostName()+", port => "+remote.getPort());
            }
            //本地地址
            SocketAddress localSocketAddress = socket.getLocalSocketAddress();
            if (localSocketAddress instanceof InetSocketAddress) {
                InetSocketAddress local  = (InetSocketAddress) localSocketAddress;
                System.out.println("local:  address => "+local.getAddress()+",name => "+local.getHostName()+", port => "+local.getPort());
            }
            System.out.println("客户端启动：host: "+socketAddress.getAddress());
            ThreadTool threadTool = ThreadTool.getInstance();
            threadTool.submit(new ReaderHandler(socket));
            threadTool.submit(new WriterHandler(socket));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
