import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ThreadTool {

    private static class Inner {
        static final ThreadTool INSTANCE = new ThreadTool();
    }

    public static ThreadTool getInstance(){
        return Inner.INSTANCE;
    }


    private final ExecutorService executors = Executors.newScheduledThreadPool(5);


    public void submit(Runnable task){
        executors.submit(task);
    }
}
